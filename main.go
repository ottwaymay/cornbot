package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	telegram "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gopkg.in/yaml.v2"
)

type secrets struct {
    Token string `yaml:"token"`
}

func main() {
    var c secrets
    c.getConf()

    bot, err := telegram.NewBotAPI(c.Token)
    if err != nil {
        panic(err)
    }

    bot.Debug = true
    updateConfig := telegram.NewUpdate(0)
	updateConfig.Timeout = 5
	updates := bot.GetUpdatesChan(updateConfig)

	for update := range updates {
                

        if update.Message == nil {
            continue
        }

        if !update.Message.IsCommand() { // ignore any non-command Messages
            continue
        }

        if update.Message.IsCommand() {
            switch update.Message.Command() {
            case "add":
                output := strings.TrimPrefix(update.Message.Text, "/add ")
                add(output)
                length := len(read())
                message := output + " added! There's " + fmt.Sprint(length) +  " items on the list!"
                msg := telegram.NewMessage(update.Message.Chat.ID, message)
                msg.ReplyToMessageID = update.Message.MessageID

                _, err := bot.Send(msg)
                if err != nil {
                    fmt.Println(err)
                }

            case "remove":
                output := strings.TrimPrefix(update.Message.Text, "/remove ")
                found := remove(output)
                length := len(read())
                message := ""
                if found == true {
                    message = output + " removed! There's " + fmt.Sprint(length) +  " items on the list!"
                } else {
                    message = output + " not found in the list! There's " + fmt.Sprint(length) +  " items on the list!"
                }

                msg := telegram.NewMessage(update.Message.Chat.ID, message)
                msg.ReplyToMessageID = update.Message.MessageID

                _, err := bot.Send(msg)
                if err != nil {
                    fmt.Println(err)
                }

            case "clear":
                clear()
                message :="List cleared! There's nothing on the list!"
                msg := telegram.NewMessage(update.Message.Chat.ID, message)
                msg.ReplyToMessageID = update.Message.MessageID

                _, err := bot.Send(msg)
                if err != nil {
                    fmt.Println(err)
                }

            case "show":
                message := "Item's On List:\n"
                input := read()
                for _, l := range(input) {
                    message = message + l + "\n"
                }
                msg := telegram.NewMessage(update.Message.Chat.ID, message)
                msg.ReplyToMessageID = update.Message.MessageID
                _, err := bot.Send(msg)
                if err != nil {
                    fmt.Println(err)
                }

            default:
                msg := telegram.NewMessage(update.Message.Chat.ID, ";~; I don't understand")
                msg.ReplyToMessageID = update.Message.MessageID
                _, err := bot.Send(msg)
                if err != nil {
                    fmt.Println(err)
                }
            }
        }
    }
}

func read() []string {
    var r []string 
    input, err := os.Open("storage.txt")

    if err != nil {
        log.Fatal(err)
    }
    defer input.Close()

    scanner := bufio.NewScanner(input)
    for scanner.Scan() {
        r = append(r,scanner.Text())
    }

    return r
}

func write(r []string) { 
    var towrite string = ""

    for i := range(r) {
        temp := r[i] + "\n"
        towrite = towrite + temp
    }
    err := ioutil.WriteFile("storage.txt", []byte(towrite), 0644)

    if err != nil {
        log.Fatal(err)
    }

}

func (c *secrets) getConf() *secrets {
    secretFile, err := ioutil.ReadFile("secrets.yaml")

    if err != nil {
        fmt.Println(err)
    }

    err = yaml.Unmarshal(secretFile, c)

    if err != nil {
        fmt.Println(err)
    }

    return c
}

func add(toadd string) {
    input := read()
    input = append(input, toadd)
    write(input)
}

func remove(toremove string) (found bool){
    found = false
    input := read()
    var output []string
    for i, l := range(input) {
        if found == true {
            continue
        } else {
            if l == toremove {
                found = true
                output = sliceRemove(input, i)
            } 
        }
    }
    if found == true {
        fmt.Println(output)
        write(output)
    }
    return found
}

func clear() {
    var output []string 
    write(output)
}

func show() {

}

func sliceRemove(s []string, i int) []string {
    return append(s[:i], s[i+1:]...)
}